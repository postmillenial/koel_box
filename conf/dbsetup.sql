CREATE DATABASE koel DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;
CREATE USER 'koel-db-user'@'localhost' IDENTIFIED BY 'koel-pass';
GRANT ALL PRIVILEGES ON koel.* TO 'koel-db-user'@'localhost' WITH GRANT OPTION;

